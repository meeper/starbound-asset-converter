# Starbound Asset Converter

A JAVA CLI utility used to convert the necessary assets from the Starbound game by Chucklefish to files used by the [Starbound Crafting Map](https://gitlab.com/meeper/starbound-crafting-map) web application.

## Installation
This tool was never meant to run on its own or by a normal user.  This utility was generally run within an Eclispe JAVA environment on a Linux desktop.  Minor tweaks to be made over time are to be expected.

## Usage
To use this utility, follow these steps:
1. Download and install the Starbound application (again, I only ever did this on Linux, YMMV).
2. Use the official game modding instructions to extract the game assets: [Unpacking Game Files](https://starbounder.org/Help:Unpacking_Game_Files)
3. Move the extracted game assets into the RawData folder included with this utility.
4. Execute this utility as a CLI application.  Again, I did this within Eclipse on a Linux desktop.
5. Copy the results from the CompiledData directory into the appropriate data directory in the website.

## Authors and acknowledgment
Shout out to those who maintain the Chucklefish forum and wiki!

## License
I'm going to call it the "don't bother me and I'm going to do what I want with it" license for this utility (is that the MIT license?).  Starbound itself is, of course, a commercial closed source application from Chucklefish.

## Project status
Well it exists.  I wrote this years ago while playing Starbound.  I'll occasionally update the game assets using this utility but there's currently nothing currently planned for this utility.  Any changes to this utility will probably be a result of changes needed by the [Starbound Crafting Map](https://gitlab.com/meeper/starbound-crafting-map) project.
