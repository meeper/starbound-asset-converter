/**
 * 
 */
package starboundassetconverter.data;

import java.util.ArrayList;

/**
 * @author e20epkt
 *
 */
public class ItemDirty {

	
	//set by json data - both object and item types
	private String rarity;
	private int price;
	private String category;
	private String inventoryIconFile;  //deliberately named differently for custom parsing
	private String description;
	private String shortdescription;
	
	//set by json data - object type
	private String objectName;
	private ArrayList<String> colonyTags;
	private boolean printable;
	private String race;
	private String apexDescription;
	private String avianDescription;
	private String floranDescription;
	private String glitchDescription;
	private String humanDescription;
	private String hylotlDescription;
	private String novakidDescription;
	
	//set by json data - item type
	private String itemName;
	private String image;
	private float foodValue;
	private String tooltipKind;
	private int maxStack;
	private float rottingMultiplier;
	


	/**
	 * Get the name of this recipe
	 * @return item name.
	 */
	public String getKey() {
		if (this.objectName != null) {
			return this.objectName;
		}
		else if (this.itemName != null) {
			return this.itemName;
		}
		else {
			return null;
		}
		
	}
	
	
	// Auto generated code -----------------------------------
	
	@Override
	public String toString() {
		return "Item [rarity=" + rarity + ", price=" + price + ", category=" + category
				+ ", inventoryIcon=" /*+ inventoryIcon*/ + ", description=" + description + ", shortDescription="
				+ shortdescription + ", objectName=" + objectName + ", colonyTags=" + colonyTags + ", printable="
				+ printable + ", race=" + race + ", apexDescription=" + apexDescription + ", avianDescription="
				+ avianDescription + ", floranDescription=" + floranDescription + ", glitchDescription="
				+ glitchDescription + ", humanDescription=" + humanDescription + ", hylotlDescription="
				+ hylotlDescription + ", novakidDescription=" + novakidDescription + ", itemName=" + itemName
				+ ", foodValue=" + foodValue + ", tooltipKind=" + tooltipKind + ", maxStack=" + maxStack
				+ ", rottingMultiplier=" + rottingMultiplier + "]";
	}

	/**
	 * @return the inventoryIconFile
	 */
	public String getInventoryIconFile() {
		return inventoryIconFile;
	}

	/**
	 * @param inventoryIconFile the inventoryIconFile to set
	 */
	public void setInventoryIconFile(String inventoryIconFile) {
		this.inventoryIconFile = inventoryIconFile;
	}

	/**
	 * @return the rarity
	 */
	public String getRarity() {
		return rarity;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortdescription() {
		return shortdescription;
	}

	/**
	 * @return the objectName
	 */
	public String getObjectName() {
		return objectName;
	}

	/**
	 * @return the colonyTags
	 */
	public ArrayList<String> getColonyTags() {
		return colonyTags;
	}

	/**
	 * @return the printable
	 */
	public boolean isPrintable() {
		return printable;
	}

	/**
	 * @param printable the printable to set
	 */
	public void setPrintable(boolean printable) {
		this.printable = printable;
	}

	/**
	 * @return the race
	 */
	public String getRace() {
		return race;
	}

	/**
	 * @return the apexDescription
	 */
	public String getApexDescription() {
		return apexDescription;
	}

	/**
	 * @return the avianDescription
	 */
	public String getAvianDescription() {
		return avianDescription;
	}

	/**
	 * @return the floranDescription
	 */
	public String getFloranDescription() {
		return floranDescription;
	}

	/**
	 * @return the glitchDescription
	 */
	public String getGlitchDescription() {
		return glitchDescription;
	}

	/**
	 * @return the humanDescription
	 */
	public String getHumanDescription() {
		return humanDescription;
	}

	/**
	 * @return the hylotlDescription
	 */
	public String getHylotlDescription() {
		return hylotlDescription;
	}

	/**
	 * @return the novakidDescription
	 */
	public String getNovakidDescription() {
		return novakidDescription;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @return the foodValue
	 */
	public float getFoodValue() {
		return foodValue;
	}

	/**
	 * @return the tooltipKind
	 */
	public String getTooltipKind() {
		return tooltipKind;
	}

	/**
	 * @return the maxStack
	 */
	public int getMaxStack() {
		return maxStack;
	}

	/**
	 * @return the rottingMultiplier
	 */
	public float getRottingMultiplier() {
		return rottingMultiplier;
	}


	
	
	
	
}
