/**
 * 
 */
package starboundassetconverter.data;

import java.io.File;
import java.util.ArrayList;

/**
 * 
 * @author Daniel Foster
 *
 */
public class Recipe {

	//set locally
	private File sourceFile;
	
	//set by json data
	private ArrayList<RecipeItem> input;
	private RecipeItem output;
	private float duration;
	private String gameLocation;
	private ArrayList<String> groups;
	
	/**
	 * Get the source file path that set this objects details
	 * @return
	 */
	public File getSourceFile() {
		return sourceFile;
	}

	/**
	 * Set the source file path that set this objects details
	 * @param sourceFile
	 */
	public void setSourceFile(File sourceFile) {
		this.sourceFile = sourceFile;
	}

	/**
	 * Get the game location path
	 * @return
	 */
	public String getGameLocation() {
		return gameLocation;
	}
	
	/**
	 * Sets the game location based on the file location
	 * @param replaceAll
	 */
	public void setGameLocation(String myGameLoc) {
		this.gameLocation = myGameLoc;
	}
	
	/**
	 * Get the name of this recipe
	 * @return
	 */
	public String getName() {
		return output.getItem();
	}

	
	// Auto generated code -----------------------------------
	
	@Override
	public String toString() {
		return "Recipe [sourceFile=" + sourceFile + ", input=" + input + ", output=" + output + ", duration=" + duration
				+ ", groups=" + groups + "]";
	}

	/**
	 * @return the input
	 */
	public ArrayList<RecipeItem> getInput() {
		return input;
	}

	/**
	 * @return the output
	 */
	public RecipeItem getOutput() {
		return output;
	}

	/**
	 * @return the duration
	 */
	public float getDuration() {
		return duration;
	}

	/**
	 * @return the groups
	 */
	public ArrayList<String> getGroups() {
		return groups;
	}
	
	
}
