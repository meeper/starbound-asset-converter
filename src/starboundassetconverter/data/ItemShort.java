/**
 * 
 */
package starboundassetconverter.data;

/**
 * @author e20epkt
 *
 */
public class ItemShort {
	private String key;
	private String name;
	
	public ItemShort(String myKey, String myName) {
		
		//primary attributes
		this.key = myKey;
		this.name = myName + " [" + myKey + "]";
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
