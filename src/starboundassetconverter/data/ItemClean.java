package starboundassetconverter.data;

import java.io.File;
import java.util.ArrayList;

public class ItemClean {

	//set locally
	private transient File itemSourceFile;
	private transient File itemIconSourceFile;
	
	//primary attributes
	private String itemKey;
	private String itemName;
	private String iconFile;
	private int price;
	private String rarity;
	private String category;
	private int maxStack;
	
	//secondary attributes
	private float foodValue;
	private float rottingMultiplier;
	private boolean printable;
	private ArrayList<String> colonyTags;

	//description attributes
	private String race;
	private String description;
	private String apexDescription;
	private String avianDescription;
	private String floranDescription;
	private String glitchDescription;
	private String humanDescription;
	private String hylotlDescription;
	private String novakidDescription;

	public ItemClean(ItemDirty dirtyItem, File sourceFile) {
		//metadata
		this.itemSourceFile = sourceFile;
		this.itemIconSourceFile = this.getInventoryIconSourceFile(dirtyItem.getInventoryIconFile());

		//name cleaning
		String dirtyName = dirtyItem.getShortdescription();
		String newItemName = dirtyName
				.replaceAll("\\^orange;", "")
				.replaceAll("\\^yellow;", "")
				.replaceAll("\\^white;", "");
		
		//primary attributes
		this.itemKey = dirtyItem.getKey();
		this.itemName = newItemName;
		this.iconFile = this.getInventoryIconDestinationName();
		this.price = dirtyItem.getPrice();
		this.rarity = dirtyItem.getRarity() == null ? "Common" : dirtyItem.getRarity();
		this.category = dirtyItem.getCategory();
		this.maxStack = dirtyItem.getMaxStack();

		//secondary attributes
		this.foodValue = dirtyItem.getFoodValue();
		this.rottingMultiplier = dirtyItem.getRottingMultiplier();
		this.printable = dirtyItem.isPrintable();
		this.colonyTags = dirtyItem.getColonyTags();

		//description attributes
		this.race = dirtyItem.getRace();
		this.description = dirtyItem.getDescription();
		this.apexDescription = dirtyItem.getApexDescription();
		this.avianDescription = dirtyItem.getAvianDescription();
		this.floranDescription = dirtyItem.getFloranDescription();
		this.glitchDescription = dirtyItem.getGlitchDescription();
		this.humanDescription = dirtyItem.getHumanDescription();
		this.hylotlDescription = dirtyItem.getHylotlDescription();
		this.novakidDescription = dirtyItem.getNovakidDescription();
		
	}
	
	public ItemShort getItemShort() {
		return new ItemShort(this.itemKey, this.itemName);
	}
	
	public File getInventoryIconSourceFile(String inventoryIconFileName) {
		if (inventoryIconFileName == null) {
			return null;
		}
		int indexOfColon = inventoryIconFileName.lastIndexOf(':');
		String cleanFileNameString = (indexOfColon > 0)
				? inventoryIconFileName.substring(0, indexOfColon)
				: inventoryIconFileName;

		String baseLocation = this.itemSourceFile.getParent();
		return new File(baseLocation + File.separator + cleanFileNameString);
	}

	
	private String getInventoryIconFileExtension() {
		File baseFile = this.itemIconSourceFile;
		if (baseFile == null) {
			return null;
		}
		String baseFileName = baseFile.getName();
		int extensionStartIndex = baseFileName.lastIndexOf('.');
		return (extensionStartIndex > 0) ? baseFileName.substring(extensionStartIndex) : null;
	}
	
	public String getInventoryIconDestinationName() {
		String itemName = this.itemKey;
		String iconExt = getInventoryIconFileExtension();
		return (itemName == null || iconExt == null) ? null : itemName + iconExt;
	}
	
	// Auto generated code -----------------------------------

	public File getItemSourceFile() {
		return itemSourceFile;
	}

	public File getItemIconSourceFile() {
		return itemIconSourceFile;
	}
	
	public String getItemKey() {
		return itemKey;
	}


	public String getItemName() {
		return itemName;
	}


	public String getIconFile() {
		return iconFile;
	}


	public int getPrice() {
		return price;
	}


	public String getRarity() {
		return rarity;
	}


	public String getCategory() {
		return category;
	}


	public int getMaxStack() {
		return maxStack;
	}


	public float getFoodValue() {
		return foodValue;
	}


	public float getRottingMultiplier() {
		return rottingMultiplier;
	}


	public boolean isPrintable() {
		return printable;
	}


	public ArrayList<String> getColonyTags() {
		return colonyTags;
	}


	public String getRace() {
		return race;
	}


	public String getDescription() {
		return description;
	}


	public String getApexDescription() {
		return apexDescription;
	}


	public String getAvianDescription() {
		return avianDescription;
	}


	public String getFloranDescription() {
		return floranDescription;
	}


	public String getGlitchDescription() {
		return glitchDescription;
	}


	public String getHumanDescription() {
		return humanDescription;
	}


	public String getHylotlDescription() {
		return hylotlDescription;
	}


	public String getNovakidDescription() {
		return novakidDescription;
	}

}
