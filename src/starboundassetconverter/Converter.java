/**
 * Starbound Asset Converter for Meeper.ca game crafting map application.
 * 
 * Go into the Starbound game directory
 * 		ex:  cd ~/.steam/steam/steamapps/common/Starbound/
 * Execute the unpacker application
 * 		linux/asset_unpacker assets/packed.pak <dest_dir>
 * 		ex:  linux/asset_unpacker assets/packed.pak unpacked_assets
 * Copy all unpacked assets to RawData directory
 * Copy the interesting directories to FilteredData for keeping them in SVN
 * Run this converter to copy and modify relevant data to CompiledData
 * Copy contents of CompiledData to data directory of web application
 */
package starboundassetconverter;

import java.util.HashMap;
import java.util.HashSet;

import starboundassetconverter.data.ItemClean;
import starboundassetconverter.util.ItemOperator;
import starboundassetconverter.util.RecipeOperator;

/**
 * Main application class
 * 
 * @author Daniel Foster
 *
 */
public class Converter {

	// constants
	private static final boolean easilyReadableOutput = true;
	private static final String RECIPES_DIRECTORY_PATH = "RawData/recipes";
	private static final String RECIPES_FILE_SUFFIX = ".recipe";
	private static final String[] ITEMS_DIRECTORIES = {"RawData/items", "RawData/objects"};
	private static final String[] ITEMS_FILE_SUFFIX = { ".object", ".activeitem", ".augment", ".back", ".beamaxe",
			".chest", ".coinitem", ".consumable", ".flashlight", ".harvestingtool", ".head", ".inspectiontool",
			".instrument", ".item", ".legs", ".liqitem", ".matitem", ".miningtool", ".painttool", ".thrownitem",
			".tillingtool", ".unlock", ".wiretool" };
	private static final String RECIPE_DESTINATION_FILE = "CompiledData/recipes.full.new.json";
	private static final String ITEM_DESTINATION_FILE = "CompiledData/items.full.new.json";
	private static final String ITEM_NAMES_DESTINATION_FILE = "CompiledData/itemNames.full.new.json";
	private static final String ITEM_DESTINATION_DIR = "CompiledData/items";
	private static final String ITEM_ICON_DESTINATION_DIR = "CompiledData/icons";
	
	

	/**
	 * A method to find the items specified by a recipe but not by an item
	 * @param itemObjects
	 * @return the list of item names.
	 */
	public HashSet<String> findMissingItems(HashSet<String> recipeItemList, HashMap<String, ItemClean> itemObjects) {
		HashSet<String> missingItemNames = new HashSet<String>();

		for (String myItem : recipeItemList) {
			if (!itemObjects.containsKey(myItem)) {
				missingItemNames.add(myItem);
			}
		}
		return missingItemNames;
	}
	
	
	
	/**
	 * Main CLI application block
	 * 
	 * @param args
	 * @return
	 */
	public int run(String[] args) {
		//create operators
		RecipeOperator recipeOp = new RecipeOperator(easilyReadableOutput);
		ItemOperator itemOp = new ItemOperator(easilyReadableOutput);
		
		//load recipes
		try {
			System.out.print("Loading recipes...");
			recipeOp.populateRecipes(RECIPES_DIRECTORY_PATH, RECIPES_FILE_SUFFIX);
			System.out.println("Done.");
			System.out.println("Found and parsed " + recipeOp.getRecipeObjects().size() + " recipe definition objects.");
			System.out.println("Found a total of " + recipeOp.getRecipeItemList().size() + " unique items in either the input or output of recipes.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while parsing recipes.");
			System.out.println(e);
			System.out.println();
			return -1;
		}
		
		//load items and objects
		try {
			System.out.print("Loading items and objects...");
			itemOp.populateItems(ITEMS_DIRECTORIES, ITEMS_FILE_SUFFIX);
			System.out.println("Done.");
			System.out.println("Found and parsed " + itemOp.getItemObjects().size() + " item definition objects.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while parsing items.");
			System.out.println(e);
			System.out.println();
			return -1;
		}

		//find missing items
		System.out.print("Locating missing items...");
		HashSet<String> missingItems = findMissingItems(recipeOp.getRecipeItemList(), itemOp.getItemObjects());
		System.out.println("Done.");
		System.out.println("We are missing " + missingItems.size() + " items.  Items missing:");
		System.out.println(missingItems);
		System.out.println();

		//write recipe single file
		try {
			System.out.print("Writing single recipe file...");
			recipeOp.writeRecipes(RECIPE_DESTINATION_FILE);
			System.out.println("Done.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while writing recipe file.");
			System.out.println(e);
			System.out.println();
			return -1;
		}

		// write item single file
		try {
			System.out.print("Writing single item file...");
			itemOp.writeItemSingleFile(ITEM_DESTINATION_FILE);
			System.out.println("Done.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while writing single item file.");
			System.out.println(e);
			System.out.println();
			return -1;
		}
		
		// write item names file
		try {
			System.out.print("Writing item names file...");
			itemOp.writeItemShortFile(ITEM_NAMES_DESTINATION_FILE);
			System.out.println("Done.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while writing item names file.");
			System.out.println(e);
			System.out.println();
			return -1;
		}

		//write item many files
		try {
			System.out.print("Writing multiple item files...");
			itemOp.writeItemMultiFile(ITEM_DESTINATION_DIR);
			System.out.println("Done.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while writing multiple item file sequence.");
			System.out.println(e);
			System.out.println();
			return -1;
		}
		
		//copy item icons
		try {
			System.out.println("Copying icon files...");
			itemOp.copyItemIcons(ITEM_ICON_DESTINATION_DIR);
			System.out.println("Done.");
			System.out.println();
		} catch (Exception e) {
			System.out.println("Error while copying icons.");
			System.out.println(e);
			System.out.println();
			return -1;
		}
		
		System.out.println("Conversion complete.");
		return 0;
	}

	/**
	 * Execution entry point
	 * 
	 * @param args
	 *            CLI Arguments - Not used
	 */
	public static void main(String[] args) {
		System.exit(new Converter().run(args));
	}

}
