/**
 * 
 */
package starboundassetconverter.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import starboundassetconverter.data.Recipe;
import starboundassetconverter.data.RecipeItem;

/**
 * @author Daniel Foster
 *
 */
public class RecipeOperator {

	HashMap<String, Recipe> recipeObjects;
	HashSet<String> recipeItemList;
	private boolean easilyReadableOutput;
	
	public RecipeOperator(boolean myEasilyReadableOutput) {
		recipeObjects = new HashMap<String, Recipe>();
		recipeItemList = new HashSet<String>();
		easilyReadableOutput = myEasilyReadableOutput;
	}
	
	/**
	 * @return the recipeObjects
	 */
	public HashMap<String, Recipe> getRecipeObjects() {
		return recipeObjects;
	}

	/**
	 * @return the recipeItemList
	 */
	public HashSet<String> getRecipeItemList() {
		return recipeItemList;
	}

	/**
	 * Populates the recipe definitions
	 * @param recipeDirectory
	 * @param recipeSuffix
	 * @throws IOException
	 */
	public void populateRecipes(String recipeDirectory, String recipeSuffix) throws IOException {
		ArrayList<File> recipeFileList = DirectoryParser.getFileList(new File(recipeDirectory), recipeSuffix);

		Gson gparser = new Gson();
		for (File myFile : recipeFileList) {
			// parse json file and create recipeObj
			String jsonString = DirectoryParser.getFileContents(myFile);
			Recipe recipeObj = gparser.fromJson(jsonString, Recipe.class);
			recipeObj.setSourceFile(myFile);
			String gameLocation = myFile.getParent()
					.replaceAll("\\\\", "/")
					.replaceAll("//", "/")
					.replaceAll(recipeDirectory, "")
					.replaceAll("\\" + recipeSuffix, "")
					.replaceAll("/", " ")
					.trim()
					.replaceAll(" ", ",")
					;
			recipeObj.setGameLocation(gameLocation);
			
			//skip over entries that are for the "money" output - recipes from the refinery
			if (recipeObj.getName().equalsIgnoreCase("money")) {
				//System.out.println("Skipping over money recipe from " + myFile.getPath());
				continue;
			}
			recipeObjects.put(recipeObj.getName(), recipeObj);
			
			// populate recipe used items list
			recipeItemList.add(recipeObj.getOutput().getItem());
			for (RecipeItem inputItem : recipeObj.getInput()) {
				recipeItemList.add(inputItem.getItem());
			}
		}
	}
	
	/**
	 * This method takes the compiled HashMap for recipes and writes them out to a single file
	 * @param destinationFile
	 * @throws IOException 
	 */
	public void writeRecipes(String destinationFile) throws IOException {
		GsonBuilder gbuilder = new GsonBuilder();
		if (easilyReadableOutput) {
			gbuilder.setPrettyPrinting();
		}
		Gson gparser = gbuilder.create();
		FileWriter outputFileHandle = null;
		try {
			outputFileHandle = new FileWriter(destinationFile);
			gparser.toJson(recipeObjects, outputFileHandle);
		} finally {
			if (outputFileHandle != null) {
				outputFileHandle.close();
			}
		}
	}


	
	
	
}
