/**
 * 
 */
package starboundassetconverter.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author Daniel Foster
 *
 */
public class DirectoryParser {
	public static HashSet<String> foundSuffixList = new HashSet<String>();
	
	public static ArrayList<File> getFileList(File theDirectory, String[] suffixFilter) throws SecurityException {
		ArrayList<File> fileList = new ArrayList<File>();
		
		// read current directory list
		File[] fullDirList = theDirectory.listFiles();

		// iterate over each entry and find directory versus file
		for (File myFile : fullDirList) {
			if (myFile.isDirectory()) {
				fileList.addAll(getFileList(myFile, suffixFilter));
			} else {
				
				int lastPeriod = myFile.getName().lastIndexOf('.');
				String fileExtension = myFile.getName().substring(lastPeriod);
				foundSuffixList.add(fileExtension);
				
				for (String currentSuffix : suffixFilter) {
					if (myFile.getName().toLowerCase().contains(currentSuffix)) {
						fileList.add(myFile);
						break;
					}
				}
			}
		}
		
		return fileList;
	}
	
	public static ArrayList<File> getFileList(File theDirectory, String suffixFilter) throws SecurityException {
		String[] suffixList = {suffixFilter};
		return getFileList(theDirectory, suffixList);
	}
	
	public static String getFileContents(File theFile) throws IOException {
		//List<String> fileContents = Files.readAllLines(theFile.toPath());
		byte[] encodedData = Files.readAllBytes(theFile.toPath());
		return new String(encodedData,StandardCharsets.UTF_8);
	}
	
	public static void deleteDirectoryContents(File folder) {
		File[] files = folder.listFiles();
		if (files != null) { // some JVMs return null for empty dirs
			for (File f : files) {
				if (f.isDirectory()) {
					deleteDirectoryContents(f);
				}
				f.delete();
			}
		}
	}
}
