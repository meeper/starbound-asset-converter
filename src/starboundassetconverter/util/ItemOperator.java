/**
 * 
 */
package starboundassetconverter.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import starboundassetconverter.data.ItemDirty;
import starboundassetconverter.data.ItemClean;
import starboundassetconverter.data.ItemShort;

/**
 * @author Daniel Foster
 *
 */
public class ItemOperator {

	HashMap<String, ItemClean> itemObjects;
	private boolean easilyReadableOutput;
	
	public ItemOperator(boolean myEasilyReadableOutput) {
		itemObjects = new HashMap<String, ItemClean>();
		easilyReadableOutput = myEasilyReadableOutput;
	}
	
	/**
	 * @return the itemObjects
	 */
	public HashMap<String, ItemClean> getItemObjects() {
		return itemObjects;
	}

	/**
	 * Populates the item definitions
	 * @param itemDirectories
	 * @param itemFileSuffixes
	 * @throws IOException
	 */
	public void populateItems(String[] itemDirectories, String[] itemFileSuffixes) throws IOException {
		ArrayList<File> itemFileList = new ArrayList<File>();
		for (String currItemDirPath : itemDirectories) {
			itemFileList.addAll(DirectoryParser.getFileList(new File(currItemDirPath), itemFileSuffixes));
		}

		GsonBuilder gbuilder = new GsonBuilder();
		gbuilder.registerTypeAdapter(ItemDirty.class, new ItemGsonHandler());
		Gson gparser = gbuilder.create();
		for (File myFile : itemFileList) {
			// parse json file and create itemObj
			String jsonString = DirectoryParser.getFileContents(myFile);
			ItemDirty itemDirtyObj = gparser.fromJson(jsonString, ItemDirty.class);
			ItemClean itemObj = new ItemClean(itemDirtyObj, myFile);
			itemObjects.put(itemObj.getItemKey(), itemObj);
		}
	}

	/**
	 * This method takes the compiled HashMap for items and writes them out to a single file
	 * @throws IOException 
	 */
	public void writeItemSingleFile(String destinationFile) throws IOException {
		//write file
		GsonBuilder gbuilder = new GsonBuilder();
		if (easilyReadableOutput) {
			gbuilder.setPrettyPrinting();
		}
		Gson gparser = gbuilder.create();
		FileWriter outputFileHandle = null;
		try {
			outputFileHandle = new FileWriter(destinationFile);
			gparser.toJson(itemObjects, outputFileHandle);
		} finally {
			if (outputFileHandle != null) {
				outputFileHandle.close();
			}
		}
	}
	
	/**
	 * This method takes the compiled hashmap for items and writes a single file for each item
	 * @throws IOException 
	 */
	public void writeItemMultiFile(String itemDestinationDir) throws IOException {
		//clear directory
		DirectoryParser.deleteDirectoryContents(new File(itemDestinationDir));
		
		//create parser
		GsonBuilder gbuilder = new GsonBuilder();
		if (easilyReadableOutput) {
			gbuilder.setPrettyPrinting();
		}
		Gson gparser = gbuilder.create();
		
		//write files
		Set<String> itemObjectKeySet = itemObjects.keySet();
		for (String currItemKey : itemObjectKeySet) {
			ItemClean currItem = itemObjects.get(currItemKey);
			String destinationFileName = itemDestinationDir + "/" + currItemKey + ".json";
			
			FileWriter outputFileHandle = null;
			try {
				outputFileHandle = new FileWriter(destinationFileName);
				gparser.toJson(currItem, outputFileHandle);
			} finally {
				if (outputFileHandle != null) {
					outputFileHandle.close();
				}
			}
		}
	}
	
	/**
	 * @param destinationFile
	 * @throws IOException 
	 */
	public void writeItemShortFile(String destinationFile) throws IOException {
		ArrayList<ItemShort> itemShorts = new ArrayList<ItemShort>();
		
		Set<String> itemObjectKeySet = itemObjects.keySet();
		for (String currItemKey : itemObjectKeySet) {
			ItemClean currItem = itemObjects.get(currItemKey);
			itemShorts.add(currItem.getItemShort());
		}
		itemShorts.sort(new CompareItemShortByName());
		
		GsonBuilder gbuilder = new GsonBuilder();
		if (easilyReadableOutput) {
			gbuilder.setPrettyPrinting();
		}
		Gson gparser = gbuilder.create();
		FileWriter outputFileHandle = null;
		try {
			outputFileHandle = new FileWriter(destinationFile);
			gparser.toJson(itemShorts, outputFileHandle);
		} finally {
			if (outputFileHandle != null) {
				outputFileHandle.close();
			}
		}
		
	}
	
	/**
	 * This method copies all of the referenced icons into the destination directory.
	 * @param itemIconDestinationDir
	 * @throws IOException 
	 */
	public void copyItemIcons(String itemIconDestinationDir) throws IOException {
		//clear directory
		DirectoryParser.deleteDirectoryContents(new File(itemIconDestinationDir));
				
		Set<String> itemObjectKeySet = itemObjects.keySet();
		for (String currItemKey : itemObjectKeySet) {
			ItemClean currItem = itemObjects.get(currItemKey);
			File sourceIconFile = currItem.getItemIconSourceFile();
			File destinationIconFile = new File(itemIconDestinationDir + File.separator + currItem.getInventoryIconDestinationName());
			if (sourceIconFile == null || destinationIconFile == null) {
				continue;
			}
			if (!sourceIconFile.exists()) {
				System.out.println("Source file missing: " + sourceIconFile.toString());
			}
			else if (destinationIconFile.exists()) {
				System.out.println("Destination file already exists: " + destinationIconFile.toString());
			}
			else {
				Files.copy(sourceIconFile.toPath(), destinationIconFile.toPath());
			}
		}
	}
}
