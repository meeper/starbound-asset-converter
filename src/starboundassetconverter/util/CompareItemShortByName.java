/**
 * 
 */
package starboundassetconverter.util;

import java.util.Comparator;

import starboundassetconverter.data.ItemShort;

/**
 * @author e20epkt
 *
 */
public class CompareItemShortByName implements Comparator<ItemShort> {

	@Override
	public int compare(ItemShort arg0, ItemShort arg1) {
		return arg0.getName().compareToIgnoreCase(arg1.getName());
	}

}
