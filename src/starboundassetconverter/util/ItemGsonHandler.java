package starboundassetconverter.util;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import starboundassetconverter.data.ItemDirty;

public class ItemGsonHandler implements JsonDeserializer<ItemDirty> {
	
	@Override
	public ItemDirty deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		JsonObject rawItemObj = arg0.getAsJsonObject(); //get as some kind of json parsible object
		Gson gsonObj = new Gson();
		ItemDirty itemObj = gsonObj.fromJson(arg0, ItemDirty.class); //parse what we can automatically
		
		//custom handling for inventoryIcon
		if (rawItemObj.has("inventoryIcon")) {
			if (rawItemObj.get("inventoryIcon").isJsonArray()) {
				//parse array form
				//TODO
			}
			else {
				//parse normal entry
				itemObj.setInventoryIconFile(rawItemObj.get("inventoryIcon").getAsString());
			}
		}
		
		//custom handling for printable
		boolean isPrintable = rawItemObj.has("printable") ? rawItemObj.get("printable").getAsBoolean() : true;
		itemObj.setPrintable(isPrintable);
		
		return itemObj;
	}
}
